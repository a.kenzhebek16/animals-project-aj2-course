package java2.project.animals.Controller;
import java2.project.animals.AnimalService.AnimalService;
import java2.project.animals.hierarchy.environments.Environment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@org.springframework.stereotype.Controller
public class WebController {
    @Autowired
    AnimalService service;
    private String answer;

    @RequestMapping(value="/home",method= RequestMethod.GET)
    public String viewHomePage(Model model, @Param("keyword") String keyword) {
        List<Environment> listData = service.listAll(keyword);
        model.addAttribute("listData",listData);
        return "Watching";
    }



    @RequestMapping("/new")
    public String newHabitatPage(Model model) {
        Environment data=new Environment();
        model.addAttribute("data",data);
        return "Adding";
    }

    @RequestMapping(value = "/save", method =RequestMethod.POST)
    public String saveData(@ModelAttribute("data") Environment data,Model model) {
        String answer = service.save(data);
        if(answer.equals("OK"))
            return "redirect:/home";
        else
            setAnswer(answer);
            return "redirect:/greeting";
    }

    @GetMapping("/greeting")
    public String greeting(Model model){
        model.addAttribute("error", getAnswer());
        return "error";
    }

    @RequestMapping("/delete/{id}")
    public String deleteStudentPage(@PathVariable (name="id") Long id) {
        service.delete(id);
        return "redirect:/home";
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }


}

