package java2.project.animals;

import java2.project.animals.hierarchy.animals.Predators.Fox;
import java2.project.animals.hierarchy.environments.Cage;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public  class AnimalsApplication  {

    public static void main(String[] args){
        SpringApplication.run(AnimalsApplication.class, args);

        Cage<Fox> foxCage = new Cage<Fox>(30);

        Fox fox = new Fox();

        System.out.println(foxCage.add(fox));
    }


}
