package java2.project.animals.JPA;

import java2.project.animals.hierarchy.environments.Environment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface RepositoryJPA extends JpaRepository<Environment, Long> {
    @Query("select s from Environment  s where s.animalName like %?1%")
    public List<Environment> findAll(String keyword);
}