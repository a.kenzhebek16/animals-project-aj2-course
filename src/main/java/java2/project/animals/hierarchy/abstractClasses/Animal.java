package java2.project.animals.hierarchy.abstractClasses;
import java2.project.animals.hierarchy.environments.Habitat;
import java2.project.animals.hierarchy.interfaces.Movable;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public abstract class Animal implements Movable {
    List<Animal> animal = new ArrayList<>();
    String name = "fox";


    // Scanner scn=new Scanner(System.in);

    Habitat hab = new Habitat(50);

    public void addAnimal() throws Exception {
        for (Animal animal1 : animal) {
            animal1.addAnimal();

            if (hab.getSize() < getComfortableSpace()) {
                throw new Exception("List of animals fewer than getComfortableSpace interface");
            }
        }
    }


}