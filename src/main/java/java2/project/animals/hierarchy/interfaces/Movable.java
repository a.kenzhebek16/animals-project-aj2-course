package java2.project.animals.hierarchy.interfaces;

public interface Movable {
    int getComfortableSpace();
}
