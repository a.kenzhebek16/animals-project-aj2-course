package java2.project.animals.hierarchy.interfaces;

public interface Flyable extends Movable {
    void fly();
}
