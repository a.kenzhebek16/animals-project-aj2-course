package java2.project.animals.hierarchy.interfaces;

public interface Walkable extends Movable {
    void walk();
}
