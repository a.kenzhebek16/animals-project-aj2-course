package java2.project.animals.hierarchy.interfaces;

public interface Swimmable extends Movable {
    void swim();
}
