package java2.project.animals.hierarchy.animals.Fishes;

import java2.project.animals.hierarchy.abstractClasses.Fish;
import java2.project.animals.hierarchy.interfaces.Swimmable;

public class Shark extends Fish implements Swimmable {
    @Override
    public int getComfortableSpace() {
        return 5;
    }

    @Override
    public void swim() {

    }
}
