package java2.project.animals.hierarchy.animals.Predators;

import java2.project.animals.hierarchy.abstractClasses.Animal;
import java2.project.animals.hierarchy.interfaces.Walkable;

public class Wolf extends Animal implements Walkable {
    @Override
    public int getComfortableSpace() {
        return 2;
    }

    @Override
    public void walk() {

    }
}
