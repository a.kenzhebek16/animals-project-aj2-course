package java2.project.animals.hierarchy.animals.Fishes;

import java2.project.animals.hierarchy.abstractClasses.Fish;
import java2.project.animals.hierarchy.interfaces.Swimmable;

public class Pike extends Fish implements Swimmable {

    @Override
    public int getComfortableSpace() {
        return 2;
    }

    @Override
    public void swim() {

    }
}
