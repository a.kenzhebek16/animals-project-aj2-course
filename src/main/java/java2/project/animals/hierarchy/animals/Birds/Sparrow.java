package java2.project.animals.hierarchy.animals.Birds;

import java2.project.animals.hierarchy.abstractClasses.Bird;
import java2.project.animals.hierarchy.interfaces.Flyable;

public class Sparrow extends Bird implements Flyable {
    @Override
    public int getComfortableSpace() {
        return 1;
    }

    @Override
    public void fly() {

    }
}
