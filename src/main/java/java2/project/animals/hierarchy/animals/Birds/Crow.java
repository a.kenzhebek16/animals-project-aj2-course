package java2.project.animals.hierarchy.animals.Birds;

import java2.project.animals.hierarchy.abstractClasses.Bird;
import java2.project.animals.hierarchy.interfaces.Flyable;

public class Crow extends Bird implements Flyable {

    @Override
    public void fly() {
        System.out.println("Crow is flying.");
    }

    @Override
    public int getComfortableSpace() {
       return 1;
    }
}
