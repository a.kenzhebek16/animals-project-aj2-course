package java2.project.animals.hierarchy.animals.Fishes;

import java2.project.animals.hierarchy.abstractClasses.Animal;
import java2.project.animals.hierarchy.interfaces.Swimmable;
import java2.project.animals.hierarchy.interfaces.Walkable;

public class Turtle extends Animal implements Walkable, Swimmable {
    @Override
    public int getComfortableSpace() {
        return 2;
    }

    @Override
    public void swim() {

    }

    @Override
    public void walk() {

    }
}
