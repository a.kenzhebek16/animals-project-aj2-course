package java2.project.animals.hierarchy.animals.Birds;

import java2.project.animals.hierarchy.abstractClasses.Bird;
import java2.project.animals.hierarchy.interfaces.Flyable;

public class Ostrich extends Bird implements Flyable {
    @Override
    public int getComfortableSpace() {
        return 4;
    }

    @Override
    public void fly() {

    }
}
