package java2.project.animals.hierarchy.animals.Birds;

import java2.project.animals.hierarchy.abstractClasses.Bird;
import java2.project.animals.hierarchy.interfaces.Swimmable;
import java2.project.animals.hierarchy.interfaces.Walkable;

public class Penguin extends Bird implements Walkable, Swimmable {
    @Override
    public int getComfortableSpace() {
        return 3;
    }

    @Override
    public void swim() {

    }

    @Override
    public void walk() {

    }
}
