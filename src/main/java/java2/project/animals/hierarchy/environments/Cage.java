package java2.project.animals.hierarchy.environments;

import java2.project.animals.hierarchy.abstractClasses.Animal;
import java2.project.animals.hierarchy.interfaces.Movable;
import java2.project.animals.hierarchy.interfaces.Walkable;

import java.util.ArrayList;
import java.util.List;

public class Cage<T extends Movable & Walkable> extends Habitat {
    private int size;
    private List<T> animals;
    private int sum = 0;

    public Cage(int size) {
        super(size);
        this.size = size;
        animals = new ArrayList<>();
    }

    public int getSize() {
        return size;
    }

    public List<T> getAnimals() {
        return animals;
    }

    public String add(T animal){
        sum += animal.getComfortableSpace();
        System.out.println(sum + " : " + size);
        if(sum <= size){
            animals.add(animal);

            System.out.println("Size now is " + animals.size());
            return "Added";
        }else
            return "Full";
    }
}
