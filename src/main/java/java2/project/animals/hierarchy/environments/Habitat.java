package java2.project.animals.hierarchy.environments;


import java2.project.animals.hierarchy.abstractClasses.Animal;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
//@Entity
//@Table(name="habitat")
public class Habitat<T> {
//    @Id
//    @GeneratedValue(strategy= GenerationType.IDENTITY)
//    private Long id;
//    private String habitat;
//    private String animalName;
    private int size;
    private List<Animal> animals;
    int sum=0;

    public Habitat(int size) {
        this.size = size;
        this.animals = new ArrayList<>();
    }

    public int getSize() {
        return size;
    }

    public List<? extends Animal> getAnimals() {
        return animals;
    }

    public void add(Animal animal){
        sum += animal.getComfortableSpace();

        if(sum <= size){
            animals.add(animal);
            System.out.println();
        }else{
            System.out.println("Cell is full.");
        }
    }
}
