package java2.project.animals.AnimalService;

import java2.project.animals.JPA.RepositoryJPA;
import java2.project.animals.hierarchy.abstractClasses.Animal;
import java2.project.animals.hierarchy.animals.Birds.Crow;
import java2.project.animals.hierarchy.animals.Birds.Ostrich;
import java2.project.animals.hierarchy.animals.Birds.Sparrow;
import java2.project.animals.hierarchy.animals.Fishes.Pike;
import java2.project.animals.hierarchy.animals.Fishes.Shark;
import java2.project.animals.hierarchy.animals.Fishes.Turtle;
import java2.project.animals.hierarchy.animals.Predators.Fox;
import java2.project.animals.hierarchy.animals.Predators.Tiger;
import java2.project.animals.hierarchy.animals.Predators.Wolf;
import java2.project.animals.hierarchy.environments.Aquarium;
import java2.project.animals.hierarchy.environments.Cage;
import java2.project.animals.hierarchy.environments.Cell;
import java2.project.animals.hierarchy.environments.Environment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AnimalService {
    @Autowired
    private RepositoryJPA repositoryJPA;
    private String full;
    Cage<Fox> foxCage = new Cage<>(30);
    Cage<Wolf> wolfCage = new Cage<>(40);
    Cage<Tiger> tigerCage = new Cage<>(50);
    Cell<Crow> crowCell = new Cell<>(20);
    Cell<Ostrich> ostrichCell = new Cell<>(50);
    Cell<Sparrow> sparrowCell = new Cell<>(40);
    Aquarium<Pike> pikeAquarium = new Aquarium<>(100);
    Aquarium<Shark> sharkAquarium = new Aquarium<>(400);
    Aquarium<Turtle> turtleAquarium = new Aquarium<>(200);


    public List<Environment> listAll(String keyword){
       if (keyword!=null) {
            return repositoryJPA.findAll(keyword);
       }
            return repositoryJPA.findAll();

    }

    public String save(Environment data) {

        if(data.getHabitat().equals("Cage")){
            if(data.getAnimalType().equals("Predators")){
                if(data.getAnimalName().equals("Fox")){
                    Fox fox = new Fox();
                    foxCage.add(fox);
                }else if(data.getAnimalName().equals("Wolf")){
                    Wolf wolf = new Wolf();
                    wolfCage.add(wolf);
                }else if(data.getAnimalName().equals("Tiger")){
                    Tiger tiger = new Tiger();
                    tigerCage.add(tiger);
                }else
                    return "This animal class is not created.";
            }else
                return data.getAnimalName() + " cannot be in a Cage!";
        }else if(data.getHabitat().equals("Cell")){
            if(data.getAnimalType().equals("Birds")){
                if(data.getAnimalName().equals("Crow")){
                    Crow crow = new Crow();
                    crowCell.add(crow);
                }else if(data.getAnimalName().equals("Ostrich")){
                    Ostrich ostrich = new Ostrich();
                    ostrichCell.add(ostrich);
                }else if(data.getAnimalName().equals("Sparrow")){
                    Sparrow sparrow = new Sparrow();
                    sparrowCell.add(sparrow);
                }else
                    return "This animal class is not created.";
            }else
                return data.getAnimalName() + " cannot be in a Cell!";
        }else if(data.getHabitat().equals("Aquarium")){
            if(data.getAnimalType().equals("Fish")){
                if(data.getAnimalName().equals("Pike")){
                    Pike pike = new Pike();
                    pikeAquarium.add(pike);
                }else if(data.getAnimalName().equals("Shark")){
                    Shark shark = new Shark();
                    sharkAquarium.add(shark);
                }else if(data.getAnimalName().equals("Turtle")){
                    Turtle turtle = new Turtle();
                    turtleAquarium.add(turtle);
                }
            }else
                return data.getAnimalName() + " cannot be in a Aquarium!";
        }

        repositoryJPA.save(data);

        return "OK";
    }

    public Environment get(Long id) {
        return repositoryJPA.findById(id).get();
    }

    public void delete(Long id) {
        repositoryJPA.deleteById(id);
    }
}
